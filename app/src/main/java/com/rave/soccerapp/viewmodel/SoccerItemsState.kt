package com.rave.soccerapp.viewmodel

import com.rave.soccerapp.model.local.SoccerItem

data class SoccerItemsState(
    val isLoading: Boolean = false,
    val soccerItemsList: List<SoccerItem> = emptyList()
)
