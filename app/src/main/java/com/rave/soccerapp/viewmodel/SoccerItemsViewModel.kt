package com.rave.soccerapp.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.rave.soccerapp.model.SoccerRepo
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class SoccerItemsViewModel @Inject constructor(private val repo: SoccerRepo) : ViewModel() {
    private val _viewState = MutableStateFlow(SoccerItemsState())
    val viewState get() = _viewState.asStateFlow()

    fun fetchSoccerItems() {
        println("BEING CALLLLELD!!!!")
        _viewState.update { it.copy(isLoading = true) }
        viewModelScope.launch {
            val response = repo.getSoccerItems()
            println("RESPONSEEEE: " + response)
            _viewState.update { it.copy(isLoading = false, soccerItemsList = response) }
        }
    }
}