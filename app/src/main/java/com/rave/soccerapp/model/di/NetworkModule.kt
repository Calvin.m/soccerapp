package com.rave.soccerapp.model.di

import com.jakewharton.retrofit2.converter.kotlinx.serialization.asConverterFactory
import com.rave.soccerapp.model.remote.SoccerService
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import kotlinx.serialization.ExperimentalSerializationApi
import kotlinx.serialization.json.Json
import okhttp3.MediaType.Companion.toMediaType
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.create

@Module
@InstallIn(SingletonComponent::class)
object NetworkModule {

    @OptIn(ExperimentalSerializationApi::class)
    @Provides
    fun providesRetrofit(): Retrofit {
        return Retrofit.Builder()
            .baseUrl("https://storage.googleapis.com/cdn-og-test-api/test-task/fixtures.json/")
//            .addConverterFactory(Json.asConverterFactory("application/json".toMediaType()))
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }

    @Provides
    fun providesSoccerService(retrofit: Retrofit): SoccerService {
        return retrofit.create()
    }
}