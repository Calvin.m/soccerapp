package com.rave.soccerapp.model.local


import kotlinx.serialization.Serializable

@Serializable
data class SoccerItem(
    val awayTeam: AwayTeam,
    val competitionStage: CompetitionStage,
    val date: String,
    val homeTeam: HomeTeam,
    val id: Int,
    val state: String,
    val type: String,
    val venue: Venue
)