package com.rave.soccerapp.model.local


import kotlinx.serialization.Serializable

@Serializable
data class CompetitionStage(
    val competition: Competition,
    val leg: String,
    val stage: String
)