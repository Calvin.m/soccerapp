package com.rave.soccerapp.model.mapper

import com.rave.soccerapp.model.local.Venue
import com.rave.soccerapp.model.remote.dto.VenueDTO

class VenueMapper : Mapper<VenueDTO, Venue> {
    override fun invoke(dto: VenueDTO): Venue {
        return with(dto) {
            Venue(
                id, name
            )
        }
    }
}