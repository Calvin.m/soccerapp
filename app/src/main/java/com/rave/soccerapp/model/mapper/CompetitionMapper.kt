package com.rave.soccerapp.model.mapper

import com.rave.soccerapp.model.local.Competition
import com.rave.soccerapp.model.remote.dto.CompetitionDTO

class CompetitionMapper : Mapper<CompetitionDTO, Competition> {
    override fun invoke(dto: CompetitionDTO): Competition {
        return with(dto) {
            Competition(
                id, name
            )
        }
    }
}