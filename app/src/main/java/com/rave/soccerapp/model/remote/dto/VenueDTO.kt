package com.rave.soccerapp.model.remote.dto


import com.rave.soccerapp.model.local.Venue
import com.rave.soccerapp.model.mapper.VenueMapper
import kotlinx.serialization.Serializable

@Serializable
data class VenueDTO(
    val id: Int = 0,
    val name: String = ""
) {
    fun toVenue(mapper: VenueMapper): Venue {
        return mapper(this)
    }
}