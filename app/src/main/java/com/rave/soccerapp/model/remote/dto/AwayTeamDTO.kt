package com.rave.soccerapp.model.remote.dto


import com.rave.soccerapp.model.local.AwayTeam
import com.rave.soccerapp.model.mapper.AwayTeamMapper
import kotlinx.serialization.Serializable

@Serializable
data class AwayTeamDTO(
    val id: Int = 0,
    val name: String = "",
    val shortName: String = "",
    val abbr: String = "",
    val alias: String = ""
) {
    fun toAwayTeam(mapper: AwayTeamMapper): AwayTeam {
        return mapper(this)
    }
}