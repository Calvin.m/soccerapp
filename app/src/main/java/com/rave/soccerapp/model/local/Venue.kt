package com.rave.soccerapp.model.local


import kotlinx.serialization.Serializable

@Serializable
data class Venue(
    val id: Int,
    val name: String
)