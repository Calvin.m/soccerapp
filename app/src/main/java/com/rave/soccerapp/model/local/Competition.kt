package com.rave.soccerapp.model.local


import kotlinx.serialization.Serializable

@Serializable
data class Competition(
    val id: Int,
    val name: String
)