package com.rave.soccerapp.model.mapper

import com.rave.soccerapp.model.local.AwayTeam
import com.rave.soccerapp.model.remote.dto.AwayTeamDTO

class AwayTeamMapper : Mapper<AwayTeamDTO, AwayTeam> {
    override fun invoke(dto: AwayTeamDTO): AwayTeam = with(dto) {
        AwayTeam(
            abbr, alias, id, name, shortName
        )
    }
}