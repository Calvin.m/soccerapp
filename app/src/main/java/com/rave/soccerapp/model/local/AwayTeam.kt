package com.rave.soccerapp.model.local


import kotlinx.serialization.Serializable

@Serializable
data class AwayTeam(
    val abbr: String,
    val alias: String,
    val id: Int,
    val name: String,
    val shortName: String
)