package com.rave.soccerapp.model.remote.dto


import com.rave.soccerapp.model.local.HomeTeam
import com.rave.soccerapp.model.mapper.HomeTeamMapper
import kotlinx.serialization.Serializable

@Serializable
data class HomeTeamDTO(
    val id: Int = 0,
    val name: String = "",
    val shortName: String = "",
    val abbr: String = "",
    val alias: String = ""
) {
    fun toHomeTeam(mapper: HomeTeamMapper) : HomeTeam {
        return mapper(this)
    }
}