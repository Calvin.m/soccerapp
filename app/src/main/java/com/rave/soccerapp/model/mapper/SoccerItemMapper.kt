package com.rave.soccerapp.model.mapper

import com.rave.soccerapp.model.local.SoccerItem
import com.rave.soccerapp.model.remote.dto.SoccerItemDTO

class SoccerItemMapper : Mapper<SoccerItemDTO, SoccerItem> {
    override fun invoke(dto: SoccerItemDTO): SoccerItem = with(dto) {
        SoccerItem(
            awayTeam = awayTeam.toAwayTeam(AwayTeamMapper()),
            competitionStage = competitionStage.toCompetitionStage(CompetitionStageMapper()),
            date,
            homeTeam = homeTeam.toHomeTeam(HomeTeamMapper()),
            id,
            state,
            type,
            venue = venue.toVenue(VenueMapper())
        )
    }
}