package com.rave.soccerapp.model.mapper

import com.rave.soccerapp.model.local.CompetitionStage
import com.rave.soccerapp.model.remote.dto.CompetitionStageDTO

class CompetitionStageMapper: Mapper<CompetitionStageDTO, CompetitionStage> {
    override fun invoke(dto: CompetitionStageDTO): CompetitionStage = with(dto) {
        CompetitionStage(
            competition = competition.toCompetition(CompetitionMapper()),
            leg,
            stage
        )
    }
}