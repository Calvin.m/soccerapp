package com.rave.soccerapp.model.mapper

interface Mapper<in DTO, out ENTITY> {
    /**
     * Invoke.
     *
     * @param dto
     * @return
     */
    operator fun invoke(dto: DTO): ENTITY
}
