package com.rave.soccerapp.model.remote.dto

import kotlinx.serialization.Serializable

@Serializable
class SoccerItemsResponseDTO : ArrayList<SoccerItemDTO>()