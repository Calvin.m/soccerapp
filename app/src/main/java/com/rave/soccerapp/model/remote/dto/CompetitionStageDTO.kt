package com.rave.soccerapp.model.remote.dto


import com.rave.soccerapp.model.local.CompetitionStage
import com.rave.soccerapp.model.mapper.CompetitionStageMapper
import kotlinx.serialization.Serializable

@Serializable
data class CompetitionStageDTO(
    val competition: CompetitionDTO = CompetitionDTO(),
    val stage: String = "",
    val leg: String = ""
) {
    fun toCompetitionStage(mapper: CompetitionStageMapper) : CompetitionStage {
        return mapper(this)
    }
}