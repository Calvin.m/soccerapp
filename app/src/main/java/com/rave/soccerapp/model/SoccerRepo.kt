package com.rave.soccerapp.model

import com.rave.soccerapp.model.local.SoccerItem
import com.rave.soccerapp.model.local.SoccerItemsResponse
import com.rave.soccerapp.model.mapper.SoccerItemMapper
import com.rave.soccerapp.model.remote.SoccerService
import com.rave.soccerapp.model.remote.dto.SoccerItemsResponseDTO
import javax.inject.Inject

class SoccerRepo @Inject constructor(
    private val service: SoccerService
) {
    val soccerItemMapper: SoccerItemMapper = SoccerItemMapper()

    suspend fun getSoccerItems(): List<SoccerItem> {
        println("about to call service -->")
        val soccerItemsResponseDto = service.getSoccerItems()
        println("DTO RESPONSEEEEE:"+soccerItemsResponseDto)
        return soccerItemsResponseDto.map {
            soccerItemMapper(it)
        }
    }
}