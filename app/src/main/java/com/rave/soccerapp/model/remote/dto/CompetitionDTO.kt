package com.rave.soccerapp.model.remote.dto


import com.rave.soccerapp.model.local.Competition
import com.rave.soccerapp.model.mapper.CompetitionMapper
import kotlinx.serialization.Serializable

@Serializable
data class CompetitionDTO(
    val id: Int = 0,
    val name: String = ""
) {
    fun toCompetition(mapper: CompetitionMapper): Competition {
        return mapper(this)
    }
}