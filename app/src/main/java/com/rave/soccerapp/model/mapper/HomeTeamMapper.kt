package com.rave.soccerapp.model.mapper

import com.rave.soccerapp.model.local.HomeTeam
import com.rave.soccerapp.model.remote.dto.HomeTeamDTO

class HomeTeamMapper : Mapper<HomeTeamDTO, HomeTeam> {
    override fun invoke(dto: HomeTeamDTO): HomeTeam {
        return with(dto) {
            HomeTeam(
                abbr, alias, id, name, shortName
            )
        }
    }
}