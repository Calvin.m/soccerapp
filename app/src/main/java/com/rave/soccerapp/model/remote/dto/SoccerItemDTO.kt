package com.rave.soccerapp.model.remote.dto


import kotlinx.serialization.Serializable

@Serializable
data class SoccerItemDTO(
    val id: Int = 0,
    val type: String = "",
    val homeTeam: HomeTeamDTO = HomeTeamDTO(),
    val awayTeam: AwayTeamDTO = AwayTeamDTO(),
    val date: String = "",
    val competitionStage: CompetitionStageDTO = CompetitionStageDTO(),
    val venue: VenueDTO = VenueDTO(),
    val state: String = ""
)