package com.rave.soccerapp.model.remote

import com.rave.soccerapp.model.remote.dto.SoccerItemsResponseDTO
import retrofit2.http.GET

interface SoccerService {
    @GET("https://storage.googleapis.com/cdn-og-test-api/test-task/fixtures.json")
    suspend fun getSoccerItems(): SoccerItemsResponseDTO
}