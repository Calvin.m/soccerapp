package com.rave.soccerapp.ui.theme

import androidx.compose.ui.graphics.Color

val Pink80 = Color(0xFFEFB8C8)

val Pink40 = Color(0xFF7D5260)

val NavyBlue = Color(0xFF051D42)
val Grey = Color(0xFF707070)