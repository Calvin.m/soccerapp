package com.rave.soccerapp.view

import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.*
import androidx.compose.material3.Card
import androidx.compose.material3.Divider
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.drawBehind
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import java.text.SimpleDateFormat
import java.util.*

@Composable
fun SoccerItemCard(
    competitionName: String,
    venueName: String,
    date: String,
    homeTeamName: String,
    awayTeamName: String,
    itemState: String
) {
    val dtStart = date
    val format = SimpleDateFormat("yyyy-MM-dd")
    val dateFormatted: Date = format.parse(dtStart)
    val dateElems = dateFormatted.toString().split(" ")
    val longDate = "${dateElems[1]} ${dateElems[2]}, ${dateElems[5]}"
    val time = dtStart.split("T")[1].split(".")[0].dropLast(3)
    Box(
        modifier = Modifier
            .fillMaxWidth()
            .drawBehind {
                val strokeWidth = 1 * density
                val y = size.height - strokeWidth / 2

                drawLine(
                    Color.LightGray,
                    Offset(0f, y),
                    Offset(size.width, y),
                    strokeWidth
                )
            }
    ) {
        Column(
            modifier = Modifier.padding(12.dp)
        ) {

            Row() {
                Column(modifier = Modifier.weight(4f)) {
                    Text(competitionName, fontWeight = FontWeight.Bold, fontSize = 20.sp, color = MaterialTheme.colorScheme.secondary)
                    Row() {
                        Text(venueName + " | ", color = MaterialTheme.colorScheme.secondary)
                        if (itemState == "postponed") {
                            Text("$longDate at $time", color = MaterialTheme.colorScheme.error)
                        } else {
                            Text("$longDate at $time", color = MaterialTheme.colorScheme.secondary)
                        }
                    }
                }
                Column(modifier = Modifier.weight(1f)) {
                    if (itemState == "postponed") {
                        Box(
                            Modifier
                                .background(MaterialTheme.colorScheme.error)
                                .padding(vertical = 4.dp, horizontal = 4.dp)
                        ) {
                            Text(text = itemState, color = MaterialTheme.colorScheme.background)
                        }
                    }
                }
            }

            Row() {
                Column(modifier = Modifier.weight(6f)) {
                    Text(
                        homeTeamName,
                        fontWeight = FontWeight.Bold,
                        fontSize = 20.sp,
                        modifier = Modifier.padding(vertical = 12.dp),
                        color = MaterialTheme.colorScheme.primary
                    )
                    Text(
                        awayTeamName,
                        fontWeight = FontWeight.Bold,
                        fontSize = 20.sp,
                        modifier = Modifier.padding(vertical = 12.dp),
                        color = MaterialTheme.colorScheme.primary
                    )
                }
                Column(
                    verticalArrangement = Arrangement.Center,
                    horizontalAlignment = Alignment.CenterHorizontally,
                    modifier = Modifier.weight(2f)
//                        .border(BorderSt)
                        .drawBehind {
                            val firstNum = 240.625
                            val secondNum = 2.75
                            val strokeWidth = 1 * density
                            println("stokWWWWW" + strokeWidth)
                            val y = size.height - strokeWidth / 2
                            println("yyy" + y)

                            drawLine(
                                Color.LightGray,
                                Offset(0f, 0f),
                                Offset(secondNum.toFloat(), firstNum.toFloat()),
                                strokeWidth
                            )
                        }
                        .padding(start = 8.dp)
                ) {
                    Text(dateElems[1], fontSize = 38.sp)
                    Text(dateElems[0].uppercase(), fontSize = 24.sp)
                }
            }

        }
    }
}